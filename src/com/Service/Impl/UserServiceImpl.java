package com.Service.Impl;

import com.Dao.UserDao;
import com.Service.UserService;
import com.entity.User;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl() {

    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }


    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Boolean login(String userName, String password) {
        List<User> users = userDao.USERS();
        for( User user : users){
            if(user.getUsername().equals(userName)&&user.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }



}

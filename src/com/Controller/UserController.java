package com.Controller;
import com.Service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

public class UserController {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        UserService userService = (UserService) applicationContext.getBean("userService");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号");
        String userName = scanner.next();
        System.out.println("请输入密码");
        String password = scanner.next();
        Boolean loginFlag = userService.login(userName,password);
        if(loginFlag) System.out.println("登陆成功");
        else System.out.println("账号或密码错误");
    }
}

package com.Dao.Impl;

import com.Dao.UserDao;
import com.Utils.DatabaseUtils;
import com.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class UserDaoImpl implements UserDao {
    public UserDaoImpl() {
    }

    @Override
    public List<User> USERS() {
        String sql = "SELECT * FROM user";
        List<User> users = new ArrayList<>();
        try(Connection connection = DatabaseUtils.getSQLConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery()){
            while (resultSet.next()){
                User user = new User(resultSet.getInt("id")
                        ,resultSet.getString("userName")
                        ,resultSet.getString("password"));
                users.add(user);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return users;
    }
}
